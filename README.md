### proton

接入层，处理面向客户端的链接以及封包。

### neutron

业务处理层，在此实现，不通消息的不同逻辑处理。私聊、群聊等。

### electron

API接口，用于对接第三方服务。


### session

使用redis作为session存储，每个连接到proton的客户端都会记录在redis中。

也就是说，凡是在redis中存在的用户id，都是在线状态。


