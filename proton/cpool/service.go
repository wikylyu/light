package cpool

import (
	"context"
	"time"

	gproto "github.com/golang/protobuf/proto"
	"gitlab.gnome.org/wikylyu/light/proto"
	"gitlab.gnome.org/wikylyu/light/proton/service"
	"gitlab.gnome.org/wikylyu/light/x"
)

func (p *CPool) KickOff(ctx context.Context, in *service.KickOffRequest) (*service.VOID, error) {
	appId := in.AppId
	userid := in.UserId

	c := p.Del(appId, userid)

	if c != nil {
		c.Write(&proto.Message{
			Id:      x.UUID(),
			Type:    proto.Message_Notification,
			Version: p.ver,
			Ctime:   time.Now().UnixNano(),
			Notification: &proto.Notification{
				Type: proto.Notification_KickOff,
			},
		})
		c.Close()
	}
	return &service.VOID{}, nil
}

func (p *CPool) SendMessage(ctx context.Context, in *service.SendMessageRequest) (*service.SendMessageReply, error) {
	appid := in.AppId
	userid := in.UserId

	c := p.Get(appid, userid)

	if c == nil {
		return &service.SendMessageReply{
			Status: service.SendMessageReply_NotFound,
		}, nil
	}

	var m proto.Message
	if err := gproto.Unmarshal(in.Message, &m); err != nil {
		return &service.SendMessageReply{
			Status: service.SendMessageReply_Error,
		}, nil
	}

	if err := c.Write(&m); err != nil {
		defer p.DelAndClose(appid, userid)
		return &service.SendMessageReply{
			Status: service.SendMessageReply_Error,
		}, nil
	}

	return &service.SendMessageReply{
		Status: service.SendMessageReply_OK,
	}, nil
}
