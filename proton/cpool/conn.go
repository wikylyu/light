package cpool

import (
	"errors"
	"time"

	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/proto"
	"gitlab.gnome.org/wikylyu/light/queue"
	"gitlab.gnome.org/wikylyu/light/session"
	"gitlab.gnome.org/wikylyu/light/x"
)

var (
	ConnTimeout = time.Minute * 5
)

var (
	ErrMessageInvalid = errors.New("invalid message")
)

func (p *CPool) connloop(appId, userId string, c proto.Conn) {
	defer p.DelAndClose(appId, userId)

	queue.PubEvent(appId, userId, p.rpcAddr, queue.EventTypeConnect)

	for {
		if err := session.SetSession(appId, userId, p.rpcAddr, ConnTimeout); err != nil {
			log.Errorf("SetSession Error:%v", err)
			break
		}
		c.SetReadDeadline(time.Now().Add(ConnTimeout))
		if m, err := c.Read(); err != nil {
			break
		} else if m.Id == "" || m.Version != proto.Version_V1 {
			log.Warnf("Invalid Message: %v", m)
		} else {
			var err error
			m.AppId = appId
			m.FromUserId = userId
			m.Ctime = time.Now().UnixNano()
			switch m.Type {
			case proto.Message_Ping:
				err = p.handlePing(c, m)
			case proto.Message_ContentMessage:
				err = p.handleContentMessage(c, m)
			case proto.Message_GroupContentMessage:
				err = p.handleGroupContentMessage(c, m)
			case proto.Message_MessageAck:
				err = p.handleMessageAck(c, m)
			}
			if err != nil { /* 消息处理出错，很可能是客户端接入的协议有问题，断开链接 */
				log.Warnf("Message Error: %v", m)
				break
			}
		}
	}
}

func (p *CPool) handlePing(c proto.Conn, m *proto.Message) error {
	if m.Ping == nil {
		return ErrMessageInvalid
	}
	pm := &proto.Message{
		Version: p.ver,
		Id:      x.UUID(),
		Type:    proto.Message_Pong,
		Ctime:   time.Now().UnixNano(),
		Pong: &proto.Pong{
			Seq: m.Ping.Seq,
		},
	}
	return c.Write(pm)
}

func (p *CPool) handleContentMessage(c proto.Conn, m *proto.Message) error {
	if m.ContentMessage == nil || m.ToUserId == "" || m.ContentMessage.Content == nil {
		return ErrMessageInvalid
	}

	queue.PubMessage(m)
	return nil
}

func (p *CPool) handleGroupContentMessage(c proto.Conn, m *proto.Message) error {
	if m.ContentMessage == nil || m.ToUserId == "" || m.ContentMessage.Content == nil {
		return ErrMessageInvalid
	}

	queue.PubMessage(m)
	return nil
}

func (p *CPool) handleMessageAck(c proto.Conn, m *proto.Message) error {
	if m.MessageAck == nil || m.MessageAck.MsgId == "" {
		return ErrMessageInvalid
	}

	queue.PubMessage(m)
	return nil
}
