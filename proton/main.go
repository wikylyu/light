package main

import (
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.gnome.org/wikylyu/light/cache"
	"gitlab.gnome.org/wikylyu/light/config"
	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/proto"
	"gitlab.gnome.org/wikylyu/light/proton/cpool"
	"gitlab.gnome.org/wikylyu/light/proton/service"
	"gitlab.gnome.org/wikylyu/light/queue"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	AppName = "proton"
)

func init() {
	initConfig()
	initLog()
	initDatabase()
	initCache()
	initQueue()
}

func initConfig() {
	config.Init(AppName)
}

func initLog() {
	log.Init()
}

func initDatabase() {
	host := config.GetString("db.mysql.host")
	user := config.GetString("db.mysql.user")
	password := config.GetString("db.mysql.password")
	dbname := config.GetString("db.mysql.dbname")
	database.InitMySQL(host, dbname, user, password)

	url := config.GetString("db.mongo.url")
	dbname = config.GetString("db.mongo.dbname")
	database.InitMongoDB(url, dbname)
}

func initCache() {
	addr := config.GetString("cache.redis.addr")
	password := config.GetString("cache.redis.password")
	db := config.GetInt("cache.redis.db")
	cache.Init(addr, password, db)
}

func initQueue() {
	url := config.GetString("queue.rabbitmq.url")
	queue.Init(url)
}

func main() {
	listenAddr := config.GetString("listen")
	rpcListenAddr := config.GetString("rpc.listen")
	wsListenAddr := config.GetString("ws.listen")

	listener, err := net.Listen("tcp", listenAddr)
	if err != nil {
		panic(err)
	}

	rpcListener, err := net.Listen("tcp", rpcListenAddr)
	if err != nil {
		panic(err)
	}

	cp := cpool.New(rpcListenAddr)
	defer cp.Close()

	go func() {
		log.Infof("listen proto on %s", listenAddr)
		for {
			if c, err := listener.Accept(); err != nil {
				log.Errorf("accept error %v", err)
				/* 如果Accept出错，等待1秒后再接受新的连接 */
				time.Sleep(time.Second)
			} else {
				go cp.Wait4Auth(proto.NewTCPConn(c))
			}
		}
	}()

	/* 启用websocket */
	if wsListenAddr != "" {
		upgrader := websocket.Upgrader{
			CheckOrigin: func(*http.Request) bool {
				return true
			},
		}
		http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
			if c, err := upgrader.Upgrade(w, r, nil); err != nil {
				log.Warnf("websocket upgrade error:%v", err)
			} else {
				go cp.Wait4Auth(proto.NewWSConn(c))
			}
		})
		go func() {
			log.Infof("listen websocket on %s", wsListenAddr)
			if err := http.ListenAndServe(wsListenAddr, nil); err != nil {
				panic(err)
			}
		}()
	}

	/* 注册grpc */
	s := grpc.NewServer()
	service.RegisterProtonServer(s, cp)
	reflection.Register(s)

	go func() {
		if err := s.Serve(rpcListener); err != nil {
			panic(err)
		}
	}()

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, os.Kill)
	<-c
}
