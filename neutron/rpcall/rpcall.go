package rpcall

import (
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/proto"
	"gitlab.gnome.org/wikylyu/light/proton/service"
	"gitlab.gnome.org/wikylyu/light/session"
)

/* 将m发送给appid.userid */
func SendMessage(appid, userid string, m *proto.Message) bool {
	saddr := session.GetSession(appid, userid)
	if saddr == "" {
		log.Infof("%s.%s not online", appid, userid)
		return false
	}
	return SendMessageTo(appid, userid, saddr, m)
}

func SendMessageTo(appid, userid, saddr string, m *proto.Message) bool {
	ok := false
	status, err := service.SendMessage(saddr, appid, userid, m)
	if err != nil {
		log.Errorf("SendMessage error:%v", err)
	} else if status != service.SendMessageReply_OK {
		log.Errorf("SendMessage failed:%v", status)
	} else {
		ok = true
	}
	return ok
}
