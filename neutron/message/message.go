package message

import (
	gproto "github.com/golang/protobuf/proto"
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/proto"
	"gitlab.gnome.org/wikylyu/light/queue"
)

func Run() {
	notifies, err := queue.MessageChannel().Consume(queue.MessageQueueName, "neutron", true, false, true, false, nil)
	if err != nil {
		panic(err)
	}

	for {
		for notify := range notifies {
			var m proto.Message
			if err := gproto.Unmarshal(notify.Body, &m); err != nil {
				log.Errorf("Unmarshal error:%v", err)
				continue
			}
			switch m.Type {
			case proto.Message_ContentMessage:
				go handleContentMessage(&m)
			case proto.Message_GroupContentMessage:
				go handleGroupContentMessage(&m)
			case proto.Message_MessageAck:
				go handleMessageAck(&m)
			}
		}
	}
}
