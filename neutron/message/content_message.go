package message

import (
	"time"

	frienddb "gitlab.gnome.org/wikylyu/light/database/friend"
	groupdb "gitlab.gnome.org/wikylyu/light/database/group"
	messagedb "gitlab.gnome.org/wikylyu/light/database/message"
	sessiondb "gitlab.gnome.org/wikylyu/light/database/session"
	userdb "gitlab.gnome.org/wikylyu/light/database/user"
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/neutron/rpcall"
	"gitlab.gnome.org/wikylyu/light/proto"
	"gitlab.gnome.org/wikylyu/light/x"
)

/* 处理私聊消息 */
func handleContentMessage(m *proto.Message) {
	log.Infof("ContentMessage: %v", m.Id)
	if m.ContentMessage == nil {
		return
	}
	appId := m.AppId
	fromUserId := m.FromUserId
	toUserId := m.ToUserId

	pm := &proto.Message{
		Version:    m.Version,
		Id:         x.UUID(),
		AppId:      appId,
		FromUserId: "",
		ToUserId:   fromUserId,
		Type:       proto.Message_MessageAck,
		Ctime:      time.Now().UnixNano(),
		MessageAck: &proto.MessageAck{
			Status: proto.MessageAck_NotFriend,
			MsgId:  m.Id,
		},
	}

	go sessiondb.UpsertSession(appId, fromUserId, sessiondb.STypePerson, toUserId)

	allowStranger := false
	app, _ := userdb.GetAppByIdCached(appId)
	if app != nil { /* 除非数据库或者缓存出问题，否则一定会有app */
		allowStranger = app.AllowStranger
	}

	if !allowStranger { /* 不允许陌生人发送消息，则检查是否加为好友 */
		friend, err := frienddb.GetFriendByUFIdCached(appId, toUserId, fromUserId)
		if err != nil || friend == nil {
			ok := rpcall.SendMessage(appId, fromUserId, pm)
			messagedb.InsertContentMessageAck(pm, ok)
			return
		}
	}

	go func() {
		messagedb.InsertContentMessage(m, false)
		rpcall.SendMessage(appId, toUserId, m)
	}()

	pm.MessageAck.Status = proto.MessageAck_OK
	ok := rpcall.SendMessage(appId, fromUserId, pm)
	messagedb.InsertContentMessageAck(pm, ok)
}

/* 处理群消息 */
func handleGroupContentMessage(m *proto.Message) {
	log.Infof("GroupContentMessage: %v", m.Id)
	if m.ContentMessage == nil {
		return
	}

	appId := m.AppId
	fromUserId := m.FromUserId
	toGroupId := m.ToGroupId

	pm := &proto.Message{
		Version:    m.Version,
		Id:         x.UUID(),
		AppId:      appId,
		FromUserId: "",
		ToUserId:   fromUserId,
		Type:       proto.Message_MessageAck,
		Ctime:      time.Now().UnixNano(),
		MessageAck: &proto.MessageAck{
			Status: proto.MessageAck_NotInGroup,
			MsgId:  m.Id,
		},
	}

	go sessiondb.UpsertSession(appId, fromUserId, sessiondb.STypeGroup, toGroupId)

	gu, err := groupdb.GetGroupUserByGUId(appId, toGroupId, fromUserId)
	if err != nil {
		log.Errorf("GetGroupUserByGUId error: %v", err)
		return
	} else if gu == nil { /* 发送方并不在群里 */
		ok := rpcall.SendMessage(appId, fromUserId, pm)
		messagedb.InsertContentMessageAck(pm, ok)
		return
	}

	users, err := groupdb.FindGroupUsers(appId, toGroupId)
	if err != nil {
		log.Errorf("FindGroupUsers error: %v", err)
		return
	}
	for _, u := range users {
		if u.UserId == fromUserId {
			continue
		}
		pm := *m
		go func(appId, userId string, m *proto.Message) {
			m.Id = x.UUID()
			m.ToUserId = userId
			messagedb.InsertContentMessage(m, false)
			rpcall.SendMessage(appId, userId, m)
		}(appId, u.UserId, &pm)
	}

	pm.MessageAck.Status = proto.MessageAck_OK
	ok := rpcall.SendMessage(appId, fromUserId, pm)
	messagedb.InsertContentMessageAck(pm, ok)
}

/* 处理聊天消息的相应 */
func handleMessageAck(m *proto.Message) {
	log.Infof("MessageAck: %v", m.Id)
	if m.MessageAck == nil {
		return
	}
	appId := m.AppId
	fromUserId := m.FromUserId

	if mm, err := messagedb.GetMessageById(m.MessageAck.MsgId); err != nil {
		return
	} else if mm == nil {
		return
	} else {
		messagedb.UpdateMessageSent(mm.Id, true)

		if mm.Type == int(proto.Message_ContentMessage) {
			sessiondb.UpsertSession(appId, fromUserId, sessiondb.STypePerson, mm.FromUserId)
		} else if mm.Type == int(proto.Message_GroupContentMessage) {
			sessiondb.UpsertSession(appId, fromUserId, sessiondb.STypeGroup, mm.ToGroupId)
		}
	}
}
