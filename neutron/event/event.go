package event

import (
	"encoding/json"
	"time"

	messagedb "gitlab.gnome.org/wikylyu/light/database/message"
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/neutron/rpcall"
	"gitlab.gnome.org/wikylyu/light/proto"
	"gitlab.gnome.org/wikylyu/light/queue"
)

func Run() {
	notifies, err := queue.EventChannel().Consume(queue.EventQueueName, "neutron", true, false, true, false, nil)
	if err != nil {
		panic(err)
	}

	for {
		for notify := range notifies {
			var e queue.Event
			if err := json.Unmarshal(notify.Body, &e); err != nil {
				log.Errorf("Unmarshal error:%v", err)
				continue
			}
			appid := e.AppId
			userid := e.UserId
			addr := e.Addr
			if e.Type == queue.EventTypeConnect {
				handleUserConnect(appid, userid, addr)
			}
		}
	}
}

func handleUserConnect(appid, userid, saddr string) {
	messages, _ := messagedb.FindMessageUnsent(appid, userid, time.Now().AddDate(0, -1, 0))
	for _, message := range messages {
		m := &proto.Message{
			Version:    proto.Version(message.Version),
			Id:         message.Id,
			FromUserId: message.FromUserId,
			ToUserId:   message.ToUserId,
			Type:       proto.Message_Type(message.Type),
			Ctime:      message.CTime.UnixNano(),
		}
		needAck := true
		if message.Type == int(proto.Message_ContentMessage) || message.Type == int(proto.Message_GroupContentMessage) {
			m.ContentMessage = &proto.ContentMessage{
				Type:    proto.ContentMessage_Type(message.ContentMessage.Type),
				Content: message.ContentMessage.Content,
			}
		} else if message.Type == int(proto.Message_MessageAck) {
			m.MessageAck = &proto.MessageAck{
				Status: proto.MessageAck_Status(message.MessageAck.Status),
				MsgId:  message.MessageAck.MsgId,
			}
		} else if message.Type == int(proto.Message_Notification) {
			m.Notification = &proto.Notification{
				Type:    proto.Notification_Type(message.Notification.Type),
				Content: message.Notification.Content,
				NeedAck: message.Notification.NeedAck,
			}
			needAck = message.Notification.NeedAck
		} else {
			continue
		}
		if ok := rpcall.SendMessageTo(appid, userid, saddr, m); !ok {
			return
		} else if !needAck { /* 不需要ACK且已经发送成功 */
			messagedb.UpdateMessageSent(message.Id, true)
		}
	}

}
