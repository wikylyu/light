package main

import (
	"os"
	"os/signal"

	"gitlab.gnome.org/wikylyu/light/cache"
	"gitlab.gnome.org/wikylyu/light/config"
	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/neutron/event"
	"gitlab.gnome.org/wikylyu/light/neutron/message"
	"gitlab.gnome.org/wikylyu/light/queue"
)

const (
	AppName = "neutron"
)

func init() {
	initConfig()
	initLog()
	initCache()
	initDatabase()
	initQueue()
}

func initConfig() {
	config.Init(AppName)
}

func initLog() {
	log.Init()
}

func initQueue() {
	url := config.GetString("queue.rabbitmq.url")
	queue.Init(url)
}

func initCache() {
	addr := config.GetString("cache.redis.addr")
	password := config.GetString("cache.redis.password")
	db := config.GetInt("cache.redis.db")
	cache.Init(addr, password, db)
}

func initDatabase() {
	host := config.GetString("db.mysql.host")
	user := config.GetString("db.mysql.user")
	password := config.GetString("db.mysql.password")
	dbname := config.GetString("db.mysql.dbname")
	database.InitMySQL(host, dbname, user, password)

	url := config.GetString("db.mongo.url")
	dbname = config.GetString("db.mongo.dbname")
	database.InitMongoDB(url, dbname)
}
func main() {
	go message.Run()
	go event.Run()

	log.Infof("%s started", AppName)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, os.Kill)
	<-c
}
