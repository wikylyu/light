package config

import (
	"fmt"

	"github.com/spf13/viper"
)

func Init(appName string) error {
	viper.SetConfigName(appName)
	viper.AddConfigPath("/etc/" + appName)
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	return nil
}

func GetString(key string) string {
	return viper.GetString(key)
}

func GetInt(key string) int {
	return viper.GetInt(key)
}

func GetStringSlice(key string) []string {
	return viper.GetStringSlice(key)
}

func GetBool(key string) bool {
	return viper.GetBool(key)
}
