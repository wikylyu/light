import { AuthResponseStatus, MessageType, Proto_AuthRequest } from "./proto";


/* 链接状态 */
export const WebSocketState = {
    Unconnected: 0,
    Connected: 1,
    Authed: 2,
};

/* Websocket链接 */
export class IMConn {
    constructor(host, token) {
        this.state = WebSocketState.Unconnected;
        this.host = host;
        this.token = token;
        this.reconnect();
    }

    onwsopen() {
        this.state = WebSocketState.Connected;
        this.sendMessage(Proto_AuthRequest(this.token));
    }

    onwsmessage(e) {
        const m = JSON.parse(e.data);
        if (this.state === WebSocketState.Connected) {  /* 已链接，但未认证 */
            if (m.type !== MessageType.AuthResponse) {
                this.close('unexpected message');
                return;
            }
            if (m.authResp.status !== AuthResponseStatus.OK) {
                this.close('invalid auth');
                return;
            }
            this.state = WebSocketState.Authed;
            return;
        }
        if (!this.onmessage) {
            return;
        }
        this.onmessage(m);
    }

    onwserror() {
        this.close('error');
    }

    onwsclose() {
        this.close('close');
    }

    /* 重新链接 */
    reconnect() {
        this.ws = new WebSocket(this.host);
        this.ws.onopen = this.onwsopen;
        this.ws.onmessage = this.onwsmessage;
        this.ws.onerror = this.onwserror;
        this.ws.onclose = this.onwsclose;
    }

    /* 关闭套接字 */
    close(err) {
        this.state = WebSocketState.Unconnected;
        this.ws.onopen = null;
        this.ws.onmessage = null;
        this.ws.onerror = null;
        this.ws.onclose = null;
        this.ws.close();
        if (this.onclose) {
            this.onclose(err);
        }
    }

    /* 发送消息 */
    sendMessage(m) {
        if (this.state < WebSocketState.Connected) {
            return false;
        }
        this.ws.send(JSON.stringify(m));
        return true;
    }
};
