
export const Version = 1;
export var AppId = '';

export const Init = (appId) => {
    AppId = appId;
};

/* 消息类型 */
export const MessageType = {
    Ping: 1,
    Pong: 2,

    AuthRequest: 1001,
    AuthResponse: 1002,
    ContentMessage: 1003,
    GroupContentMessage: 1004,
    RoomContentMessage: 1005,
    MessageAck: 1006,
    Notification: 1007,
};

/* 通知类型 */
export const NotificationType = {
    Undefined: 0,
    KickOff: 1,
    FriendRequest: 2,
};

/* 认证状态 */
export const AuthResponseStatus = {
    Undefined: 0,
    OK: 1,
    AppNotFound: 2,
    TokenNotFound: 3,
};

export const Proto_AuthRequest = (token, client = '') => {
    return {
        version: Version,
        appId: AppId,
        type: MessageType.AuthRequest,
        authReq: {
            client: client,
            token: token,
        }
    }
};
