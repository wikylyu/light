package gim

import (
	"fmt"
	"net"

	"gitlab.gnome.org/wikylyu/light/proto"
	"gitlab.gnome.org/wikylyu/light/x"
)

func NewClient(appid, addr string) *Client {
	return &Client{
		appid: appid,
		addr:  addr,
		c:     nil,
	}
}

func (c *Client) reconnect() error {
	if c.c != nil {
		return nil
	}
	conn, err := net.Dial("tcp", c.addr)
	if err != nil {
		return err
	}
	c.c = proto.NewTCPConn(conn)
	return nil
}

func (c *Client) Read() (*proto.Message, error) {
	m, err := c.c.Read()
	if err != nil {
		return nil, err
	}
	if m.Type == proto.Message_ContentMessage {
		pm := proto.Message{
			Version: proto.Version_V1,
			Type:    proto.Message_ContentMessageAck,
			Id:      x.UUID(),
			ContentMessageAck: &proto.ContentMessageAck{
				Status: proto.ContentMessageAck_OK,
				MsgId:  m.Id,
			},
		}
		if err := c.Write(&pm); err != nil {
			return nil, err
		}
	}
	return m, nil
}

func (c *Client) Write(m *proto.Message) error {
	return c.c.Write(m)
}

func (c *Client) Auth(token, client string) error {
	if err := c.reconnect(); err != nil {
		return err
	}
	m := &proto.Message{
		Id:      x.UUID(),
		Type:    proto.Message_AuthRequest,
		Version: proto.Version_V1,
		AuthReq: &proto.AuthRequest{
			AppId:  c.appid,
			Token:  token,
			Client: client,
		},
	}
	err := c.c.Write(m)
	if err != nil {
		c.close()
		return err
	}

	m, err = c.c.Read()
	if err != nil {
		c.close()
		return err
	}
	if m.Type != proto.Message_AuthResponse || m.AuthResp == nil {
		return proto.ErrInvalidMessage
	} else if m.AuthResp.Status != proto.AuthResponse_OK {
		return fmt.Errorf("auth failed:%v", m.AuthResp.Status)
	}
	return nil
}

func (c *Client) close() {
	c.c.Close()
	c.c = nil
}
