package gim

import (
	"gitlab.gnome.org/wikylyu/light/proto"
)

type Client struct {
	appid string
	addr  string
	c     proto.Conn
}
