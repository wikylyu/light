conn = new Mongo();
db = conn.getDB("light");
// 创建索引，这些索引非常重要
db.message.ensureIndex({
    appId: 1,
    toUserId: 1,
    sent: 1,
    ctime: 1
});
db.message.ensureIndex({
    id: 1
});
db.message.ensureIndex({
    id: 1,
    type: 1,
});

db.session.ensureIndex({
    appId: 1,
    userId: 1,
    type: 1,
    targetId: 1,
});
db.session.ensureIndex({
    appId: 1,
    userId: 1,
    utime: 1,
});