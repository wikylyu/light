package queue

import (
	"github.com/golang/protobuf/proto"
	"github.com/streadway/amqp"
	"gitlab.gnome.org/wikylyu/light/log"
)

func PubMessage(m proto.Message) {
	body, err := proto.Marshal(m)
	if err != nil {
		log.Errorf("Marshal error:%v", err)
		return
	}
	data := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "application/octet-stream",
		Type:         "light/message",
		Body:         body,
	}
	err = _messageChannel.Publish(MessageExchangeName, MessageRouterKey, false, false, data)
	if err != nil {
		log.Errorf("PubMessage error:%v", err)
	}
}

func MessageChannel() *amqp.Channel {
	return _messageChannel
}
