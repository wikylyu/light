package queue

import (
	"github.com/streadway/amqp"
)

const (
	MessageQueueName    = "lightmq"
	MessageExchangeName = "lightmex"
	MessageRouterKey    = "light.message"

	EventQueueName    = "lighteq"
	EventExchangeName = "lighteex"
	EventRouterKey    = "light.event"
)

var (
	_conn           *amqp.Connection
	_messageChannel *amqp.Channel
	_eventChannel   *amqp.Channel
)

func newChannel(conn *amqp.Connection, exchangeName, queueName, routerKey string) (*amqp.Channel, error) {
	ch, err := conn.Channel()
	if err != nil {
		return nil, err
	}

	if err := ch.ExchangeDeclare(exchangeName, amqp.ExchangeTopic, true, false, false, false, nil); err != nil {
		return nil, err
	}

	if q, err := ch.QueueDeclare(queueName, true, false, false, false, nil); err != nil {
		return nil, err
	} else if err := ch.QueueBind(q.Name, routerKey, exchangeName, false, nil); err != nil {
		return nil, err
	}

	if err := ch.Qos(2, 0, false); err != nil {
		return nil, err
	}
	return ch, nil
}

func Init(mqurl string) error {
	var err error
	_conn, err = amqp.Dial(mqurl)
	if err != nil {
		panic(err)
	}

	_messageChannel, err = newChannel(_conn, MessageExchangeName, MessageQueueName, MessageRouterKey)
	if err != nil {
		panic(err)
	}
	_eventChannel, err = newChannel(_conn, EventExchangeName, EventQueueName, EventRouterKey)
	if err != nil {
		panic(err)
	}

	return nil
}
