package queue

import (
	"encoding/json"

	"github.com/streadway/amqp"
	"gitlab.gnome.org/wikylyu/light/log"
)

const (
	EventTypeConnect = 1
)

type Event struct {
	Type   int    `json:"type"`
	AppId  string `json:"app_id"`
	UserId string `json:"user_id"`
	Addr   string `json:"addr"`
}

func PubEvent(appid, userid, addr string, et int) {
	e := Event{
		Type:   et,
		AppId:  appid,
		UserId: userid,
		Addr:   addr,
	}
	body, err := json.Marshal(e)
	if err != nil {
		log.Errorf("Marshal error:%v", err)
		return
	}
	msg := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "application/json",
		Type:         "light/event",
		Body:         body,
	}
	err = _eventChannel.Publish(EventExchangeName, EventRouterKey, false, false, msg)
	if err != nil {
		log.Errorf("PubEvent error:%v", err)
	}
}

func EventChannel() *amqp.Channel {
	return _eventChannel
}
