package errors

import (
	"fmt"
	"net/http"
)

/* 这里的错误全部都是对客户端的 */
var (
	ErrInternalException = NewError(http.StatusInternalServerError, -1, "服务器错误")
	ErrSourceInvalid     = NewError(http.StatusBadRequest, -2, "客户端未定义")
	ErrRequestInvalid    = NewError(http.StatusBadRequest, -3, "参数错误")

	/* 帐号相关 */
	ErrAccountNotFound       = NewError(http.StatusNotFound, 10003, "用户不存在")
	ErrPhoneAlreadyExists    = NewError(http.StatusConflict, 10004, "手机号已经存在")
	ErrPhoneNotFound         = NewError(http.StatusNotFound, 10006, "手机号不存在")
	ErrPhoneInvalid          = NewError(http.StatusBadRequest, 10001, "手机号格式不正确")
	ErrPasswordInvalid       = NewError(http.StatusBadRequest, 10003, "密码格式不正确")
	ErrCodeInvalid           = NewError(http.StatusBadRequest, 10002, "验证码错误")
	ErrPasswordIncorrect     = NewError(http.StatusForbidden, 10007, "密码错误")
	ErrFriendRequestNotFound = NewError(http.StatusNotFound, 10008, "好友请求不存在")

	/* 文件不存在  */
	ErrFileNotFound = NewError(http.StatusBadRequest, 20001, "文件不存在 ")
)

func NewError(status, code int, msg interface{}) *Error {
	return &Error{
		HttpStatus: status,
		Status:     code,
		Message:    fmt.Sprintf("%v", msg),
	}
}

func CopyError(e *Error) *Error {
	err := new(Error)
	*err = *e
	return err
}

func CopyErrorWithMsg(e *Error, msg string) *Error {
	err := CopyError(e)
	err.Message = msg
	return err
}

type Error struct {
	HttpStatus int    `json:"-"`
	Status     int    `json:"status"`
	Message    string `json:"message"`
}

func (e *Error) Error() string {
	return e.Message
}
