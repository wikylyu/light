package electron

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func post(path string, body interface{}) error {
	url := e.Host + path

	data, err := json.Marshal(body)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	if err != nil {
		return err
	}
	req.Header.Add("X-APP", e.AppId)
	req.Header.Add("X-Secret", e.AppKey)
	req.Header.Add("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	result := struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
	}{}
	if err := json.Unmarshal(data, &result); err != nil {
		return err
	} else if result.Status != 0 {
		return fmt.Errorf("status = %d", result.Status)
	}

	return nil
}
