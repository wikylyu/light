package account

import (
	"github.com/labstack/echo"
	"gitlab.gnome.org/wikylyu/light/laser/controller/context"
	"gitlab.gnome.org/wikylyu/light/laser/errors"
	accountModel "gitlab.gnome.org/wikylyu/light/laser/model/account"
)

func Signup(c echo.Context) error {
	ctx := c.(*context.Context)
	var req SignupRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}
	nickname := req.Nickname
	avatar := req.Avatar
	phone := req.Phone
	password := req.Password

	a, err := accountModel.GetAccountByPhone(phone)
	if err != nil {
		return errors.ErrInternalException
	} else if a != nil {
		return errors.ErrPhoneAlreadyExists
	}

	a, err = accountModel.CreateAccount(phone, password, nickname, avatar)
	if err != nil {
		return errors.ErrInternalException
	}

	t, err := accountModel.CreateToken(a.Id)
	if err != nil {
		return errors.ErrInternalException
	}

	return ctx.SUCCESS(t)
}

func Login(c echo.Context) error {
	ctx := c.(*context.Context)
	var req LoginRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	phone := req.Phone
	password := req.Password

	a, err := accountModel.GetAccountByPhone(phone)
	if err != nil {
		return errors.ErrInternalException
	} else if a == nil {
		return errors.ErrPhoneNotFound
	} else if !a.VerifyPassword(password) {
		return errors.ErrPasswordIncorrect
	}

	t, err := accountModel.CreateToken(a.Id)
	if err != nil {
		return errors.ErrInternalException
	}

	return ctx.SUCCESS(t)
}

func GetAccount(c echo.Context) error {
	ctx := c.(*context.Context)

	return ctx.SUCCESS(ctx.Account)
}

func GetUser(c echo.Context) error {
	ctx := c.(*context.Context)

	userId := ctx.ParamInt("id")
	a, err := accountModel.GetAccountById(uint64(userId))
	if err != nil {
		return errors.ErrInternalException
	} else if a == nil {
		return errors.ErrAccountNotFound
	}
	return ctx.SUCCESS(a)
}

func SearchUsers(c echo.Context) error {
	ctx := c.(*context.Context)

	s := ctx.QueryParam("s")

	users, err := accountModel.SearchUsers(s, ctx.Account.Id)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(users)
}
