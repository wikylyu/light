package account

type SignupRequest struct {
	Nickname string `json:"nickname" form:"nickname" query:"nickname" validate:"gt=0"`
	Avatar   string `json:"avatar" form:"avatar" query:"avatar" validate:"gt=0"`
	Phone    string `json:"phone" form:"phone" query:"phone" validate:"gt=0"`
	Password string `json:"password" form:"password" query:"password" validate:"gt=0"`
}

type LoginRequest struct {
	Phone    string `json:"phone" form:"phone" query:"phone" validate:"gt=0"`
	Password string `json:"password" form:"password" query:"password" validate:"gt=0"`
}
