package context

import (
	"github.com/labstack/echo"
	accountModel "gitlab.gnome.org/wikylyu/light/laser/model/account"
)

type Context struct {
	echo.Context
	Token   *accountModel.Token
	Account *accountModel.Account
}
