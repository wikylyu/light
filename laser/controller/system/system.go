package system

import (
	"github.com/labstack/echo"
	"gitlab.gnome.org/wikylyu/light/laser/controller/context"
	"gitlab.gnome.org/wikylyu/light/laser/electron"
)

func GetSystemConfig(c echo.Context) error {
	ctx := c.(*context.Context)

	appId := electron.GetAppId()

	cfg := map[string]interface{}{
		"appId": appId,
	}

	return ctx.SUCCESS(cfg)
}
