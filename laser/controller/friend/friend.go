package friend

import (
	"fmt"

	"github.com/labstack/echo"
	"gitlab.gnome.org/wikylyu/light/laser/controller/context"
	"gitlab.gnome.org/wikylyu/light/laser/electron"
	"gitlab.gnome.org/wikylyu/light/laser/errors"
	accountModel "gitlab.gnome.org/wikylyu/light/laser/model/account"
	friendModel "gitlab.gnome.org/wikylyu/light/laser/model/friend"
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/proto"
)

func CreateFriendRequest(c echo.Context) error {
	ctx := c.(*context.Context)
	var req CreateFriendRequestRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	userId := ctx.Account.Id
	friendId := req.UserId
	message := req.Message

	f, err := friendModel.GetUserFriend(userId, friendId)
	if err != nil {
		return errors.ErrInternalException
	} else if f != nil || userId == friendId {
		return ctx.SUCCESS(nil)
	}

	if _, err := friendModel.CreateFriendRequest(userId, friendId, message); err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(nil)
}

func GetFriendRequest(c echo.Context) error {
	ctx := c.(*context.Context)
	id := ctx.ParamInt("id")
	if id <= 0 {
		return errors.ErrRequestInvalid
	}

	fr, err := friendModel.GetFriendRequest(uint64(id))
	if err != nil {
		return errors.ErrInternalException
	} else if fr == nil {
		return errors.ErrFriendRequestNotFound
	}

	a, err := accountModel.GetAccountById(fr.UserId)
	if err != nil {
		return errors.ErrInternalException
	} else if a == nil {
		return errors.ErrAccountNotFound
	}

	r := FriendRequestResponse{
		FriendRequest: fr,
		UserId:        a.Id,
		Nickname:      a.Nickname,
		Avatar:        a.Avatar,
	}

	return ctx.SUCCESS(r)
}

func AcceptFriendRequest(c echo.Context) error {
	ctx := c.(*context.Context)
	id := ctx.ParamInt("id")
	if id <= 0 {
		return errors.ErrRequestInvalid
	}

	fr, err := friendModel.GetFriendRequest(uint64(id))
	if err != nil {
		return errors.ErrInternalException
	} else if fr == nil || fr.FriendId != ctx.Account.Id {
		return errors.ErrFriendRequestNotFound
	}

	if err := friendModel.AddUserFriend(fr.UserId, fr.FriendId); err != nil {
		return errors.ErrInternalException
	}

	if err := electron.SendContentMessage(fmt.Sprintf("%d", fr.FriendId), fmt.Sprintf("%d", fr.UserId), int(proto.ContentMessage_Text), map[string]string{"text": fr.Message}); err != nil {
		log.Errorf("SendNotification Error: %v", err)
		return err
	}

	return ctx.SUCCESS(nil)
}

func FindUserFriends(c echo.Context) error {
	ctx := c.(*context.Context)

	users, err := friendModel.FindUserFriends(ctx.Account.Id)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(users)
}
