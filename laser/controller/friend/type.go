package friend

import (
	friendModel "gitlab.gnome.org/wikylyu/light/laser/model/friend"
)

type CreateFriendRequestRequest struct {
	UserId  uint64 `json:"user_id" form:"user_id" query:"user_id" validate:"gt=0"`
	Message string `json:"message" form:"message" query:"message"`
}

type FriendRequestResponse struct {
	*friendModel.FriendRequest
	Nickname string `json:"nickname"`
	Avatar   string `json:"avatar"`
	UserId   uint64 `json:"user_id"`
}
