package middleware

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.gnome.org/wikylyu/light/laser/controller/context"
	"gitlab.gnome.org/wikylyu/light/laser/errors"
	accountModel "gitlab.gnome.org/wikylyu/light/laser/model/account"
)

func AuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.(*context.Context)
		tokenID := ctx.GetToken()
		if tokenID == "" {
			return c.NoContent(http.StatusUnauthorized)
		}
		token, err := accountModel.GetTokenById(tokenID)
		if err != nil {
			return errors.ErrInternalException
		} else if token == nil {
			return c.NoContent(http.StatusUnauthorized)
		}
		account, err := accountModel.GetAccountById(token.UserId)
		if err != nil {
			return errors.ErrInternalException
		} else if account == nil {
			return c.NoContent(http.StatusUnauthorized)
		}
		ctx.Token = token
		ctx.Account = account
		return next(ctx)
	}
}
