package middleware

import (
	"github.com/labstack/echo"
	"gitlab.gnome.org/wikylyu/light/laser/controller/context"
)

func ContextMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := &context.Context{
			Context: c,
		}
		return next(ctx)
	}
}
