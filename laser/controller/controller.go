package controller

import (
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo"
	"gitlab.gnome.org/wikylyu/light/laser/controller/account"
	"gitlab.gnome.org/wikylyu/light/laser/controller/file"
	"gitlab.gnome.org/wikylyu/light/laser/controller/friend"
	"gitlab.gnome.org/wikylyu/light/laser/controller/middleware"
	"gitlab.gnome.org/wikylyu/light/laser/controller/system"
	"gitlab.gnome.org/wikylyu/light/laser/errors"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func errorHandler(err error, c echo.Context) {
	if e, ok := err.(*errors.Error); ok {
		c.JSON(e.HttpStatus, e)
	} else if e, ok := err.(*echo.HTTPError); ok {
		c.JSON(e.Code, errors.NewError(0, -1, e.Message))
	} else {
		c.JSON(http.StatusInternalServerError, errors.NewError(0, -1, err.Error()))
	}
}

func Register(e *echo.Echo) {
	e.Validator = &CustomValidator{validator: validator.New()}
	e.HTTPErrorHandler = errorHandler
	api := e.Group("/api", middleware.ContextMiddleware)
	authAPI := api.Group("", middleware.AuthMiddleware)

	api.GET("/system", system.GetSystemConfig)
	api.POST("/signup", account.Signup)
	api.POST("/login", account.Login)
	api.POST("/file", file.UploadFile)
	api.GET("/file/:id", file.DownloadFile)
	authAPI.GET("/account", account.GetAccount)
	authAPI.GET("/users", account.SearchUsers)
	authAPI.GET("/user/:id", account.GetUser)

	authAPI.POST("/friend/request", friend.CreateFriendRequest)
	authAPI.GET("/friend/request/:id", friend.GetFriendRequest)
	authAPI.PUT("/friend/request/:id", friend.AcceptFriendRequest)
	authAPI.GET("/friends", friend.FindUserFriends)
}
