package friend

import "time"

type Friend struct {
	Id       uint64    `xorm:"id pk autoincr" json:"id"`
	UserId   uint64    `xorm:"user_id" json:"user_id"`
	FriendId uint64    `xorm:"friend_id" json:"friend_id"`
	UTime    time.Time `xorm:"utime" json:"utime"`
	CTime    time.Time `xorm:"ctime" json:"ctime"`
}

func (*Friend) TableName() string {
	return "friend"
}

type FriendRequest struct {
	Id       uint64    `xorm:"id pk autoincr" json:"id"`
	UserId   uint64    `xorm:"user_id" json:"user_id"`
	FriendId uint64    `xorm:"friend_id" json:"friend_id"`
	Message  string    `xorm:"message" json:"message"`
	UTime    time.Time `xorm:"utime" json:"utime"`
	CTime    time.Time `xorm:"ctime" json:"ctime"`
}

func (*FriendRequest) TableName() string {
	return "friend_request"
}
