package friend

import (
	"fmt"

	"github.com/go-xorm/xorm"
	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/laser/electron"
	accountModel "gitlab.gnome.org/wikylyu/light/laser/model/account"
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/proto"
)

func GetFriendRequest(id uint64) (*FriendRequest, error) {
	db := database.DB()

	fr := FriendRequest{
		Id: id,
	}
	if ok, err := db.Get(&fr); err != nil {
		if err != xorm.ErrNotExist {
			log.Errorf("GetFriendRequest Error: %v", err)
			return nil, err
		}
		return nil, nil
	} else if !ok {
		return nil, nil
	}
	return &fr, nil
}

func CreateFriendRequest(userId, friendId uint64, message string) (*FriendRequest, error) {
	tx := database.DB().NewSession()
	defer tx.Close()

	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return nil, err
	}

	fr := FriendRequest{
		UserId:   userId,
		FriendId: friendId,
		Message:  message,
	}

	if _, err := tx.Insert(&fr); err != nil {
		log.Errorf("CreateFriendRequest Error: %v", err)
		return nil, err
	}

	if err := electron.SendNotification(fmt.Sprintf("%d", friendId), int(proto.Notification_FriendRequest), map[string]string{"id": fmt.Sprintf("%d", fr.Id)}, true); err != nil {
		log.Errorf("SendNotification Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return nil, err
	}

	return &fr, nil
}

func GetUserFriend(userId, friendId uint64) (*Friend, error) {
	db := database.DB()

	f := Friend{
		UserId:   userId,
		FriendId: friendId,
	}

	if ok, err := db.Get(&f); err != nil {
		if err != xorm.ErrNotExist {
			log.Errorf("GetUserFriend Error: %v", err)
			return nil, err
		}
		return nil, nil
	} else if !ok {
		return nil, nil
	}
	return &f, nil
}

func FindUserFriends(userId uint64) ([]*accountModel.Account, error) {
	db := database.DB()

	users := make([]*accountModel.Account, 0)
	if err := db.Sql("SELECT account.* FROM account INNER JOIN friend ON friend.friend_id=account.id WHERE friend.user_id=?", userId).Find(&users); err != nil {
		log.Errorf("FindUserFriends Error: %v", err)
		return nil, err
	}
	return users, nil
}

func AddUserFriend(userId, friendId uint64) error {
	tx := database.DB().NewSession()
	defer tx.Close()
	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return err
	}

	if _, err := tx.Exec(`INSERT IGNORE INTO friend(user_id,friend_id) VALUES(?,?)`, userId, friendId); err != nil {
		log.Errorf("AddUserFriend Error: %v", err)
		return err
	}
	if _, err := tx.Exec(`INSERT IGNORE INTO friend(user_id,friend_id) VALUES(?,?)`, friendId, userId); err != nil {
		log.Errorf("AddUserFriend Error: %v", err)
		return err
	}

	if err := electron.AddUserFriend(fmt.Sprintf("%d", userId), fmt.Sprintf("%d", friendId)); err != nil {
		log.Errorf("AddUserFriend Error: %v")
		return err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return err
	}
	return nil
}
