package account

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/go-xorm/xorm"
	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/laser/electron"
	"gitlab.gnome.org/wikylyu/light/log"
)

func GetTokenById(id string) (*Token, error) {
	db := database.DB()

	var t Token
	if ok, err := db.Where("id=?", id).Get(&t); err != nil {
		if err != xorm.ErrNotExist {
			log.Errorf("GetTokenById Error: %v", err)
			return nil, err
		}
		return nil, nil
	} else if !ok {
		return nil, nil
	}
	return &t, nil
}

func CreateToken(userId uint64) (*Token, error) {
	tx := database.DB().NewSession()
	defer tx.Close()

	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return nil, err
	}

	t := Token{
		UserId: userId,
	}

	if _, err := tx.Delete(&t); err != nil {
		log.Errorf("CreateToken Error: %v", err)
		return nil, err
	}

	t.Id = bson.NewObjectId().Hex()
	if _, err := tx.Insert(&t); err != nil {
		log.Errorf("CreateToken Error: %v", err)
		return nil, err
	}

	if err := electron.CreateToken(fmt.Sprintf("%d", userId), "", t.Id); err != nil {
		log.Errorf("elctron.CreateToken Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return nil, err
	}
	return &t, nil
}
