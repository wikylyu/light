package account

import (
	"crypto/md5"
	"fmt"
	"time"
)

type Token struct {
	Id     string    `xorm:"id pk" json:"id"`
	UserId uint64    `xorm:"user_id" json:"user_id"`
	UTime  time.Time `xorm:"utime" json:"utime"`
	CTime  time.Time `xorm:"ctime" json:"ctime"`
}

func (*Token) TableName() string {
	return "token"
}

type Account struct {
	Id       uint64    `xorm:"id pk autoincr" json:"id"`
	Phone    string    `xorm:"phone" json:"phone"`
	Password string    `xorm:"password" json:"-"`
	Nickname string    `xorm:"nickname" json:"nickname"`
	Avatar   string    `xorm:"avatar" json:"avatar"`
	UTime    time.Time `xorm:"utime" json:"utime"`
	CTime    time.Time `xorm:"ctime" json:"ctime"`
}

func (*Account) TableName() string {
	return "account"
}

func (a *Account) VerifyPassword(password string) bool {
	return a.Password == fmt.Sprintf("%X", md5.New().Sum([]byte(password)))
}
