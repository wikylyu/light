package account

import (
	"crypto/md5"
	"fmt"

	"github.com/go-xorm/xorm"
	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
)

func GetAccountById(id uint64) (*Account, error) {
	db := database.DB()

	var a Account
	if ok, err := db.Where("id=?", id).Get(&a); err != nil {
		if err != xorm.ErrNotExist {
			log.Errorf("GetAccountById Error: %v", err)
			return nil, err
		}
		return nil, nil
	} else if !ok {
		return nil, nil
	}
	return &a, nil
}

func GetAccountByPhone(phone string) (*Account, error) {
	db := database.DB()

	var a Account
	if ok, err := db.Where("phone=?", phone).Get(&a); err != nil {
		if err != xorm.ErrNotExist {
			log.Errorf("GetAccountByPhone Error: %v", err)
			return nil, err
		}
		return nil, nil
	} else if !ok {
		return nil, nil
	}
	return &a, nil
}

func CreateAccount(phone, password, nickname, avatar string) (*Account, error) {
	tx := database.DB().NewSession()
	defer tx.Close()

	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return nil, err
	}

	a := Account{
		Phone:    phone,
		Password: fmt.Sprintf("%X", md5.New().Sum([]byte(password))),
		Nickname: nickname,
		Avatar:   avatar,
	}

	if _, err := tx.Insert(&a); err != nil {
		log.Errorf("CreateAccount Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return nil, err
	}
	return &a, nil
}

func SearchUsers(s string, selfId uint64) ([]*Account, error) {
	db := database.DB()

	users := make([]*Account, 0)

	q := "%" + s + "%"
	if err := db.Where(`nickname LIKE ? AND id !=?`, q, selfId).Limit(10).Find(&users); err != nil {
		log.Errorf("SearchUsers Error: %v", err)
		return nil, err
	}
	return users, nil
}
