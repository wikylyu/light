CREATE DATABASE `laser` DEFAULT CHARSET=UTF8;
USE `laser`;

CREATE TABLE `account` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `phone` VARCHAR(32) NOT NULL COMMENT '手机号码',
    `password` VARCHAR(128) NOT NULL COMMENT '手机号码',
    `nickname` VARCHAR(128) NOT NULL COMMENT '用户昵称',
    `avatar` VARCHAR(1024) NOT NULL COMMENT '头像',
    `utime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ctime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    UNIQUE(`phone`),
    INDEX(`nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1000 COLLATE=utf8_bin COMMENT='用户';

CREATE TABLE `token` (
    `id` VARCHAR(128) NOT NULL PRIMARY KEY,
    `user_id` BIGINT UNSIGNED NOT NULL COMMENT '用户Id',
    `utime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ctime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1000 COLLATE=utf8_bin COMMENT='Token';


CREATE TABLE `friend` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `user_id` BIGINT NOT NULL COMMENT '用户Id',
    `friend_id` BIGINT NOT NULL COMMENT '好友Id',
    `utime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ctime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    INDEX(`user_id`),
    UNIQUE(`user_id`, `friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1000 COLLATE=utf8_bin COMMENT='好友表';

CREATE TABLE `friend_request` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `user_id` BIGINT NOT NULL COMMENT '用户Id',
    `friend_id` BIGINT NOT NULL COMMENT '好友Id',
    `message` VARCHAR(128) NOT NULL COMMENT '申请消息',
    `status` TINYINT NOT NULL DEFAULT 0 COMMENT '请求状态，0-未处理，1-已接受',
    `utime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ctime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    INDEX(`user_id`),
    INDEX(`user_id`, `friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1000 COLLATE=utf8_bin COMMENT='好友申请表';