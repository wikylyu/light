package room

import (
	"github.com/labstack/echo"
	roomdb "gitlab.gnome.org/wikylyu/light/database/room"
	"gitlab.gnome.org/wikylyu/light/electron/controller/context"
	"gitlab.gnome.org/wikylyu/light/electron/errors"
)

type AddRoomUsersRequest struct {
	RoomId  string   `json:"room_id" form:"room_id" query:"room_id" validate:"gt=0"`
	UserIds []string `json:"user_ids" form:"user_ids" query:"user_ids" validate:"gt=0,dive,gt=0"`
	Temp    bool     `json:"temp" form:"temp" query:"temp"`
}

func AddRoomUsers(c echo.Context) error {
	ctx := c.(*context.Context)
	var req AddRoomUsersRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.Id
	roomId := req.RoomId
	userIds := req.UserIds
	temp := req.Temp

	users, err := roomdb.CreateRoomUsers(appId, roomId, userIds, temp)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(users)
}
