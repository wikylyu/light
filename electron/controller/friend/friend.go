package friend

import (
	"github.com/labstack/echo"
	frienddb "gitlab.gnome.org/wikylyu/light/database/friend"
	"gitlab.gnome.org/wikylyu/light/electron/controller/context"
	"gitlab.gnome.org/wikylyu/light/electron/errors"
)

type AddFriendRequest struct {
	UserId   string `json:"user_id" form:"user_id" query:"user_id" validate:"gt=0"`
	FriendId string `json:"friend_id" form:"friend_id" query:"friend_id" validate:"gt=0"`
}

func AddFriend(c echo.Context) error {
	ctx := c.(*context.Context)

	var req AddFriendRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.Id
	userId := req.UserId
	friendId := req.FriendId

	f, err := frienddb.CreateFriend(appId, userId, friendId)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(f)
}

type DeleteFriendRequest struct {
	UserId   string `json:"user_id" form:"user_id" query:"user_id" validate:"gt=0"`
	FriendId string `json:"friend_id" form:"friend_id" query:"friend_id" validate:"gt=0"`
}

func DeleteFriend(c echo.Context) error {
	ctx := c.(*context.Context)

	var req DeleteFriendRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.Id
	userId := req.UserId
	friendId := req.FriendId

	err := frienddb.DeleteFriend(appId, userId, friendId)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(nil)
}
