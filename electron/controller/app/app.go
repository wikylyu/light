package app

import (
	"github.com/labstack/echo"
	userdb "gitlab.gnome.org/wikylyu/light/database/user"
	"gitlab.gnome.org/wikylyu/light/electron/controller/context"
	"gitlab.gnome.org/wikylyu/light/electron/errors"
)

type CreateAppRequest struct {
	AppId          string `json:"app_id" form:"app_id" query:"app_id" validate:"gt=0"`
	Name           string `json:"name" form:"name" query:"name" validate:"gt=0"`
	AllowAnonymous bool   `json:"allow_anonymous" form:"allow_anonymous" query:"allow_anonymous"`
	AllowStranger  bool   `json:"allow_stranger" form:"allow_stranger" query:"allow_stranger"`
}

func CreateApp(c echo.Context) error {
	ctx := c.(*context.Context)
	var req CreateAppRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := req.AppId
	name := req.Name
	allowAnonymous := req.AllowAnonymous
	allowStranger := req.AllowStranger

	app, err := userdb.GetAppById(appId)
	if err != nil {
		return errors.ErrInternalException
	} else if app != nil {
		return errors.ErrAppIdExists
	}

	app, err = userdb.CreateApp(appId, name, allowAnonymous, allowStranger)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(app)
}
