package token

import (
	"github.com/labstack/echo"
	userdb "gitlab.gnome.org/wikylyu/light/database/user"
	"gitlab.gnome.org/wikylyu/light/electron/controller/context"
	"gitlab.gnome.org/wikylyu/light/electron/errors"
)

type CreateTokenRequest struct {
	UserId string `json:"user_id" form:"user_id" query:"user_id" validate:"gt=0"`
	Token  string `json:"token" form:"token" query:"token" validate:"gt=0"`
	Client string `json:"client" form:"client" query:"client"`
}

func CreateToken(c echo.Context) error {
	ctx := c.(*context.Context)
	var req CreateTokenRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.Id
	userId := req.UserId
	token := req.Token
	client := req.Client

	t, err := userdb.CreateToken(appId, userId, client, token)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(t)
}
