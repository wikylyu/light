package context

import (
	"github.com/labstack/echo"
	userdb "gitlab.gnome.org/wikylyu/light/database/user"
)

type Context struct {
	echo.Context
	App *userdb.App
}
