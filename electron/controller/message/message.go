package message

import (
	"time"

	"github.com/labstack/echo"
	messagedb "gitlab.gnome.org/wikylyu/light/database/message"
	"gitlab.gnome.org/wikylyu/light/electron/controller/context"
	"gitlab.gnome.org/wikylyu/light/electron/errors"
	"gitlab.gnome.org/wikylyu/light/neutron/rpcall"
	"gitlab.gnome.org/wikylyu/light/proto"
	"gitlab.gnome.org/wikylyu/light/x"
)

/* 发送消息 */

func SendContentMessage(c echo.Context) error {
	ctx := c.(*context.Context)

	var req SendContentMessageRequest
	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.Id
	fromUserId := req.FromUserId
	toUserId := req.ToUserId
	ntype := req.Type
	content := req.Content

	m := &proto.Message{
		Id:         x.UUID(),
		Version:    proto.Version_V1,
		AppId:      appId,
		FromUserId: fromUserId,
		ToUserId:   toUserId,
		ToGroupId:  "",
		Type:       proto.Message_ContentMessage,
		Ctime:      time.Now().UnixNano(),
		ContentMessage: &proto.ContentMessage{
			Type:    proto.ContentMessage_Type(ntype),
			Content: content,
		},
	}

	if err := messagedb.InsertContentMessage(m, false); err != nil {
		return errors.ErrInternalException
	}

	rpcall.SendMessage(appId, toUserId, m)

	return ctx.SUCCESS(m)
}

func SendNotificationMessage(c echo.Context) error {
	ctx := c.(*context.Context)
	var req SendNotificationMessageRequest

	if err := ctx.Bind(&req); err != nil {
		return errors.ErrRequestInvalid
	} else if err := ctx.Validate(&req); err != nil {
		return errors.ErrRequestInvalid
	}

	appId := ctx.App.Id
	userId := req.UserId
	ntype := req.Type
	content := req.Content

	needAck := req.NeedAck

	m := &proto.Message{
		Id:         x.UUID(),
		Version:    proto.Version_V1,
		AppId:      appId,
		FromUserId: "",
		ToUserId:   userId,
		ToGroupId:  "",
		Type:       proto.Message_Notification,
		Ctime:      time.Now().UnixNano(),
		Notification: &proto.Notification{
			Type:    proto.Notification_Type(ntype),
			Content: content,
			NeedAck: needAck,
		},
	}

	if err := messagedb.InsertNotification(m, false); err != nil {
		return errors.ErrInternalException
	}

	if rpcall.SendMessage(appId, userId, m) && !needAck {
		messagedb.UpdateMessageSent(m.Id, true)
	}

	return ctx.SUCCESS(m)
}
