package session

import (
	"github.com/labstack/echo"
	sessiondb "gitlab.gnome.org/wikylyu/light/database/session"
	"gitlab.gnome.org/wikylyu/light/electron/controller/context"
	"gitlab.gnome.org/wikylyu/light/electron/errors"
)

func FindUserSessions(c echo.Context) error {
	ctx := c.(*context.Context)

	appId := ctx.App.Id
	userId := ctx.QueryParam("user_id")

	sessions, err := sessiondb.FindUserSessions(appId, userId, 500)
	if err != nil {
		return errors.ErrInternalException
	}
	return ctx.SUCCESS(sessions)
}
