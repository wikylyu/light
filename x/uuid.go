package x

import (
	"github.com/globalsign/mgo/bson"
)

func UUID() string {
	return bson.NewObjectId().Hex()
}
