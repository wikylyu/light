

.PHONY: proto proton neutron electron


all: proton neutron electron

proto:
	make -C proto proto

proton:
	make -C proton proton

neutron:
	make -C neutron neutron

electron:
	make -C electron electron