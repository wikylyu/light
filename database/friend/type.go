package friend

import "time"

type Friend struct {
	Id       uint64    `xorm:"id pk autoincr" json:"-"`
	AppId    string    `xorm:"app_id" json:"app_id"`
	UserId   string    `xorm:"user_id" json:"user_id"`
	FriendId string    `xorm:"friend_id" json:"friend_id"`
	UTime    time.Time `xorm:"utime" json:"utime"`
	CTime    time.Time `xorm:"ctime" json:"ctime"`
}
