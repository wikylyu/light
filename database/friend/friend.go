package friend

import (
	"fmt"
	"time"

	"gitlab.gnome.org/wikylyu/light/cache"
	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
)

const (
	CacheFriendPrefix = "light.friend"
)

func CreateFriend(appId, userId, friendId string) (*Friend, error) {
	tx := database.DB().NewSession()
	defer tx.Close()

	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return nil, err
	}

	friend := Friend{}
	if ok, err := tx.Where("app_id=? AND user_id=? AND friend_id=?", appId, userId, friendId).Get(&friend); err != nil {
		log.Errorf("CreateFriend Error:%v", err)
		return nil, err
	} else if ok {
		return &friend, nil
	}

	friend.AppId = appId
	friend.UserId = userId
	friend.FriendId = friendId
	friend.UTime = time.Now()
	friend.CTime = time.Now()
	if _, err := tx.Insert(&friend); err != nil {
		log.Errorf("CreateFriend Error:%v", err)
		return nil, err
	}

	friend1 := Friend{
		AppId:    appId,
		UserId:   friendId,
		FriendId: userId,
		UTime:    time.Now(),
		CTime:    time.Now(),
	}
	if _, err := tx.Insert(&friend1); err != nil {
		log.Errorf("CreateFriend Error:%v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return nil, err
	}
	return &friend, nil
}

func DeleteFriend(appId, userId, friendId string) error {
	tx := database.DB().NewSession()
	defer tx.Close()

	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return err
	}
	friend := Friend{
		AppId:    appId,
		UserId:   userId,
		FriendId: friendId,
	}
	if _, err := tx.Delete(&friend); err != nil {
		log.Errorf("DeleteFriend Error: %v", err)
		return err
	}

	friend = Friend{
		AppId:    appId,
		UserId:   friendId,
		FriendId: userId,
	}
	if _, err := tx.Delete(&friend); err != nil {
		log.Errorf("DeleteFriend Error: %v", err)
		return err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return err
	}
	return nil
}

func GetFriendByUFId(appId, userId, friendId string) (*Friend, error) {
	db := database.DB()

	var friend Friend

	if ok, err := db.Where("app_id=? AND user_id=? AND friend_id=?", appId, userId, friendId).Get(&friend); err != nil {
		log.Errorf("GetFriendByUFId Error:%v", err)
		return nil, err
	} else if !ok {
		return nil, nil
	}
	return &friend, nil
}

func GetFriendByUFIdCached(appId, userId, friendId string) (*Friend, error) {
	key := fmt.Sprintf("%s.%s-%s-%s", CacheFriendPrefix, appId, userId, friendId)
	var friend Friend
	if ok := cache.GetJSON(key, &friend); ok {
		return &friend, nil
	} else {
		v, err := cache.Lock(key, time.Second*5)
		if err != nil {
			log.Errorf("cache.Lock error: %v", err)
		}
		friend, err := GetFriendByUFId(appId, userId, friendId)
		if err != nil {
			return nil, err
		} else if friend == nil {
			return nil, nil
		}
		if v != "" {
			cache.SetJSON(key, friend, time.Hour)
			if err := cache.Unlock(key, v); err != nil {
				log.Errorf("cache.Unlock error: %v", err)
			}
		}
		return friend, nil
	}
}
