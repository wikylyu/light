package room

import "time"

type RoomUser struct {
	Id     uint64    `xorm:"id pk autoincr" json:"-"`
	AppId  string    `xorm:"app_id" json:"app_id"`
	RoomId string    `xorm:"room_id" json:"room_id"`
	UserId string    `xorm:"user_id" json:"user_id"`
	Temp   bool      `xorm:"temp" json:"temp"`
	UTime  time.Time `xorm:"utime" json:"utime"`
	CTime  time.Time `xorm:"ctime" json:"ctime"`
}
