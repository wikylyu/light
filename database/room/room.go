package room

import (
	"time"

	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
)

func CreateRoomUsers(appId, roomId string, userIds []string, temp bool) ([]*RoomUser, error) {
	tx := database.DB().NewSession()
	defer tx.Close()

	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return nil, err
	}

	users := make([]*RoomUser, 0)
	for _, userId := range userIds {
		user := RoomUser{
			AppId:  appId,
			RoomId: roomId,
			UserId: userId,
			Temp:   temp,
		}
		if ok, err := tx.Get(&user); err != nil {
			log.Errorf("CreateRoomUsers Error: %v", err)
			return nil, err
		} else if !ok {
			user.UTime = time.Now()
			user.CTime = time.Now()
			if _, err := tx.Insert(&user); err != nil {
				log.Errorf("CreateRoomUsers Error: %v", err)
				return nil, err
			}
		}
		users = append(users, &user)
	}
	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return nil, err
	}
	return users, nil
}
