package group

import (
	"time"

	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
)

func GetGroupUserByGUId(appid, groupid, userid string) (*GroupUser, error) {
	db := database.DB()

	gu := GroupUser{}

	if ok, err := db.Where("app_id=? AND group_id=? AND user_id=?", appid, groupid, userid).Get(&gu); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	} else if !ok {
		return nil, nil
	}
	return &gu, nil
}

func FindGroupUsers(appId, groupId string) ([]*GroupUser, error) {
	db := database.DB()

	users := make([]*GroupUser, 0)

	if err := db.Where("app_id=? AND group_id=?", appId, groupId).Find(&users); err != nil {
		log.Errorf("DB Error: %v", err)
		return nil, err
	}
	return users, nil
}

func CreateGroupUsers(appId, groupId string, userIds []string) ([]*GroupUser, error) {
	tx := database.DB().NewSession()
	defer tx.Close()

	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return nil, err
	}

	users := make([]*GroupUser, 0)
	for _, userId := range userIds {
		user := GroupUser{
			AppId:   appId,
			GroupId: groupId,
			UserId:  userId,
		}
		if ok, err := tx.Get(&user); err != nil {
			log.Errorf("CreateGroupUsers Error: %v", err)
			return nil, err
		} else if !ok {
			user.UTime = time.Now()
			user.CTime = time.Now()
			if _, err := tx.Insert(&user); err != nil {
				log.Errorf("CreateGroupUsers Error: %v", err)
				return nil, err
			}
		}
		users = append(users, &user)
	}
	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return nil, err
	}
	return users, nil
}

func DeleteGroupUsers(appId, groupId string, userIds []string) error {
	tx := database.DB().NewSession()
	defer tx.Close()

	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return err
	}

	for _, userId := range userIds {
		user := GroupUser{
			AppId:   appId,
			GroupId: groupId,
			UserId:  userId,
		}
		if _, err := tx.Delete(&user); err != nil {
			log.Errorf("CreateGroupUsers Error: %v", err)
			return err
		}
	}
	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return err
	}
	return nil
}
