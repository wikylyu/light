package group

import "time"

type GroupUser struct {
	Id      uint64    `xorm:"id pk autoincr" json:"-"`
	AppId   string    `xorm:"app_id" json:"app_id"`
	GroupId string    `xorm:"group_id" json:"group_id"`
	UserId  string    `xorm:"user_id" json:"user_id"`
	UTime   time.Time `xorm:"utime" json:"utime"`
	CTime   time.Time `xorm:"ctime" json:"ctime"`
}
