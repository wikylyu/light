package session

import (
	"time"

	"github.com/globalsign/mgo/bson"
	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
)

const (
	CollectionName = "session"
)

/* 更新用户会话 */
func UpsertSession(appId, userId string, stype int, targetId string) (*Session, error) {
	c := database.Collection(CollectionName)

	session := Session{
		AppId:    appId,
		UserId:   userId,
		Type:     stype,
		TargetId: targetId,
		UTime:    time.Now(),
		CTime:    time.Now(),
	}

	query := bson.M{
		"appId":    appId,
		"userId":   userId,
		"type":     stype,
		"targetId": targetId,
	}

	if _, err := c.Upsert(query, &session); err != nil {
		log.Errorf("UpsertSession Error: %v", err)
		return nil, err
	}

	return &session, nil
}

/* 查询用户会话记录 */
func FindUserSessions(appId, userId string, n int) ([]*Session, error) {
	c := database.Collection(CollectionName)

	sessions := make([]*Session, 0)

	query := bson.M{
		"appId":  appId,
		"userId": userId,
	}

	if err := c.Find(query).Sort("-utime").Limit(n).All(&sessions); err != nil {
		log.Errorf("FindUserSessions Error: %v", err)
		return nil, err
	}
	return sessions, nil
}
