package session

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

const (
	STypePerson = 1
	STypeGroup  = 2
)

type Session struct {
	Id       bson.ObjectId `bson:"_id,omitempty" json:"-"`
	AppId    string        `bson:"appId" json:"app_id"`
	UserId   string        `bson:"userId" json:"user_id"`
	Type     int           `bson:"type" json:"type"`
	TargetId string        `bson:"targetId" json:"target_id"`
	UTime    time.Time     `bson:"utime" json:"utime"`
	CTime    time.Time     `bson:"ctime" json:"ctime"`
}

func (*Session) TableName() string {
	return "session"
}
