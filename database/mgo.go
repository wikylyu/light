package database

import (
	"github.com/globalsign/mgo"
)

var _msession *mgo.Session
var _mdb *mgo.Database

func InitMongoDB(url, database string) error {
	var err error
	_msession, err = mgo.Dial(url)
	if err != nil {
		panic(err)
	}
	_mdb = _msession.DB(database)
	return nil
}

func Collection(c string) *mgo.Collection {
	return _mdb.C(c)
}
