package user

import (
	"time"

	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
)

func GetTokenWithClient(token, client string) (*Token, error) {
	db := database.DB()

	var t Token

	if ok, err := db.Where("token=? AND client=?", token, client).Get(&t); err != nil {
		log.Errorf("GetTokenWithClient Error: %v", err)
		return nil, err
	} else if !ok {
		return nil, nil
	}
	return &t, nil
}

func CreateToken(appId, userId, client, token string) (*Token, error) {
	tx := database.DB().NewSession()
	defer tx.Close()

	if err := tx.Begin(); err != nil {
		log.Errorf("Begin Error: %v", err)
		return nil, err
	}

	t := Token{
		AppId:  appId,
		UserId: userId,
		Client: client,
	}

	if _, err := tx.Delete(&t); err != nil {
		log.Errorf("CreateToken Error: %v", err)
		return nil, err
	}

	t.Token = token
	t.CTime = time.Now()
	t.UTime = time.Now()
	if _, err := tx.Insert(&t); err != nil {
		log.Errorf("CreateToken Error: %v", err)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		log.Errorf("Commit Error: %v", err)
		return nil, err
	}

	return &t, nil
}
