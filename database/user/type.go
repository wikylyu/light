package user

import (
	"time"
)

const (
	SessionClientWeb     = 0
	SessionClientAndroid = 1
	SessionClientIOS     = 2
)

type App struct {
	Id             string    `xorm:"id pk" json:"id"`
	SecretKey      string    `xorm:"secret_key" json:"secret_key"`
	Name           string    `xorm:"name" json:"name"`
	AllowAnonymous bool      `xorm:"allow_anonymous" json:"allow_anonymous"`
	AllowStranger  bool      `xorm:"allow_stranger" json:"allow_stranger"`
	UTime          time.Time `xorm:"utime" json:"utime"`
	CTime          time.Time `xorm:"ctime" json:"ctime"`
}

func (*App) TableName() string {
	return "app"
}

type Token struct {
	Id     uint64    `xorm:"id pk autoincr" json:"-"`
	AppId  string    `xorm:"app_id" json:"app_id"`
	UserId string    `xorm:"user_id" json:"user_id"`
	Token  string    `xorm:"token" json:"token"`
	Client string    `xorm:"client" json:"client"`
	UTime  time.Time `xorm:"utime" json:"utime"`
	CTime  time.Time `xorm:"ctime" json:"ctime"`
}

func (*Token) TableName() string {
	return "token"
}
