package user

import (
	"fmt"
	"time"

	"gitlab.gnome.org/wikylyu/light/cache"
	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/x"
)

const (
	CacheAppPrefix = "light.app"
)

func GetAppById(appid string) (*App, error) {
	db := database.DB()

	var app App

	if ok, err := db.Where("id=?", appid).Get(&app); err != nil {
		log.Errorf("GetAppById Error: %v", err)
		return nil, err
	} else if !ok {
		return nil, nil
	}
	return &app, nil
}

func CreateApp(appid, name string, allowAnonymous, allowStranger bool) (*App, error) {
	db := database.DB()

	app := App{
		Id:             appid,
		SecretKey:      x.UUID(),
		Name:           name,
		AllowAnonymous: allowAnonymous,
		AllowStranger:  allowStranger,
		CTime:          time.Now(),
		UTime:          time.Now(),
	}
	if _, err := db.Insert(&app); err != nil {
		log.Errorf("CreateApp Error: %v", err)
		return nil, err
	}
	return &app, nil
}

func GetAppByIdCached(appId string) (*App, error) {
	if appId == "" {
		return nil, nil
	}
	key := fmt.Sprintf("%s.%s", CacheAppPrefix, appId)
	var app App
	if ok := cache.GetJSON(key, &app); ok {
		return &app, nil
	} else {
		v, err := cache.Lock(key, time.Second*5)
		if err != nil {
			log.Errorf("cache.Lock Error: %v", err)
		}

		app, err := GetAppById(appId)
		if err != nil {
			return nil, err
		}
		if v != "" {
			if app != nil {
				cache.SetJSON(key, app, time.Hour)
			}
			if err := cache.Unlock(key, v); err != nil {
				log.Errorf("cache.Unlock Error: %v", err)
			}
		}
		return app, nil
	}
}
