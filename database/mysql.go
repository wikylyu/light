package database

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
)

var _db *xorm.Engine

func InitMySQL(host, database, user, password string) error {
	var err error
	url := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", user, password, host, database)
	_db, err = xorm.NewEngine("mysql", url)
	if err != nil {
		panic(err)
	}
	_db.SetMaxIdleConns(50)
	_db.SetMaxOpenConns(50)
	return nil
}

func DB() *xorm.Engine {
	return _db
}
