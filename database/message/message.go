package message

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.gnome.org/wikylyu/light/database"
	"gitlab.gnome.org/wikylyu/light/log"
	"gitlab.gnome.org/wikylyu/light/proto"
)

const (
	CollectionName = "message"
)

func GetMessageById(id string) (*Message, error) {
	c := database.Collection(CollectionName)

	var m Message

	query := bson.M{
		"id": id,
	}
	if err := c.Find(query).One(&m); err != nil {
		if err != mgo.ErrNotFound {
			log.Errorf("GetMessageById Error:%v", err)
			return nil, err
		}
		return nil, nil
	}
	return &m, nil
}

/* 获取针对某一用户，未发送成功的消息 */
func FindMessageUnsent(appId, userId string, st time.Time) ([]*Message, error) {
	c := database.Collection(CollectionName)

	query := bson.M{
		"appId":    appId,
		"toUserId": userId,
		"sent":     false,
		"ctime": bson.M{
			"$gte": st,
		},
	}

	messages := make([]*Message, 0)
	if err := c.Find(query).Sort("ctime").All(&messages); err != nil {
		log.Errorf("FindMessageUnsent Error:%v", err)
		return nil, err
	}
	return messages, nil
}

func UpdateMessageSent(mid string, sent bool) error {
	c := database.Collection(CollectionName)

	query := bson.M{"id": mid}
	set := bson.M{
		"$set": bson.M{"sent": sent, "stime": time.Now()},
	}
	if err := c.Update(query, set); err != nil {
		if err != mgo.ErrNotFound {
			log.Errorf("UpdateMessageSent Error:%v", err)
			return err
		}
		log.Warnf("message %s not found", mid)
		return nil
	}
	return nil
}

func InsertMessage(m *Message) error {
	if m.Sent {
		m.STime = time.Now()
	}
	c := database.Collection(CollectionName)
	if err := c.Insert(m); err != nil {
		log.Errorf("InsertMessage Error:%v", err)
		return err
	}
	return nil
}

func InsertNotification(m *proto.Message, sent bool) error {
	bm := Message{
		Version:    int(m.Version),
		Id:         m.Id,
		Type:       int(m.Type),
		AppId:      m.AppId,
		FromUserId: m.FromUserId,
		ToUserId:   m.ToUserId,
		Sent:       sent,
		CTime:      time.Unix(0, m.Ctime),
		Notification: &Notification{
			Type:    int(m.Notification.Type),
			Content: m.Notification.Content,
			NeedAck: m.Notification.NeedAck,
		},
	}
	return InsertMessage(&bm)
}

func InsertContentMessage(m *proto.Message, sent bool) error {
	bm := Message{
		Version:    int(m.Version),
		Id:         m.Id,
		Type:       int(m.Type),
		AppId:      m.AppId,
		FromUserId: m.FromUserId,
		ToUserId:   m.ToUserId,
		Sent:       sent,
		CTime:      time.Unix(0, m.Ctime),
		ContentMessage: &ContentMessage{
			Type:    int(m.ContentMessage.Type),
			Content: m.ContentMessage.Content,
		},
	}
	return InsertMessage(&bm)
}

func InsertContentMessageAck(m *proto.Message, sent bool) error {
	bm := Message{
		Version:    int(m.Version),
		Id:         m.Id,
		Type:       int(m.Type),
		AppId:      m.AppId,
		FromUserId: m.FromUserId,
		ToUserId:   m.ToUserId,
		Sent:       sent,
		CTime:      time.Unix(0, m.Ctime),
		MessageAck: &MessageAck{
			Status: int(m.MessageAck.Status),
			MsgId:  m.MessageAck.MsgId,
		},
	}
	return InsertMessage(&bm)
}
