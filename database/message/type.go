package message

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

type Message struct {
	BId        bson.ObjectId `bson:"_id,omitempty"`
	Version    int           `bson:"version"`
	Id         string        `bson:"id"`
	AppId      string        `bson:"appId"`
	FromUserId string        `bson:"fromUserId"` /* 发送方的id，如果是服务器下发的消息，则为空 */
	ToUserId   string        `bson:"toUserId"`   /* 目标用户的Id */
	ToGroupId  string        `bson:"toGroupId"`
	Type       int           `bson:"type"`
	Sent       bool          `bson:"sent"` /* 是否已经发送成功，部分数据需要ack响应后才更新该字段 */

	STime time.Time `bson:"stime"` /* 发送成功的时间 */
	CTime time.Time `bson:"ctime"`

	ContentMessage *ContentMessage `bson:"contentMessage,omitempty"`
	MessageAck     *MessageAck     `bson:"messageAck,omitempty"`
	Notification   *Notification   `bson:"notification,omitempty"`
}

type ContentMessage struct {
	Type    int               `bson:"type"`
	Content map[string]string `bson:"content"`
}

type MessageAck struct {
	Status int    `bson:"status"`
	MsgId  string `bson:"msgId"`
}

type Notification struct {
	Type    int               `bson:"type"`
	Content map[string]string `bson:"content"`
	NeedAck bool              `bson:"needAck"`
}
